import React, {Component} from 'react';
import {StyleSheet, View, Text} from "react-native";
import Maps from './src/view/Maps'
import GetLatLong from './src/view/GetLatLong'
import SetAnnotation from './src/view/SetAnnotation'
import MapBox from '@react-native-mapbox-gl/maps'

MapBox.setAccessToken("pk.eyJ1IjoiYm9tYnVyIiwiYSI6ImNrMWJneWZlbjBpZ2ozY252Yzdjd29tcGMifQ._boZnEnBaVHQqQCwOtY52Q")

export default class App extends Component {
    render() {
        return (
            /*<View style={styles.page}>
                <View style={styles.container}>
                    <MapBox.MapView  style={styles.map}>
                        <MapBox.Camera zoomLevel={15} centerCoordinate={[106.84513, -6.21462]} />
                    </MapBox.MapView>
                </View>

            </View>*/
            <SetAnnotation/>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    container: {
        height: "100%",
        width:"100%",
    },
    map: {
        flex: 1
    }
})

