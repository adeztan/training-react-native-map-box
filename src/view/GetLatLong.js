import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import MapBox from '@react-native-mapbox-gl/maps'

MapBox.setAccessToken("pk.eyJ1IjoiYm9tYnVyIiwiYSI6ImNrMWJneWZlbjBpZ2ozY252Yzdjd29tcGMifQ._boZnEnBaVHQqQCwOtY52Q")

const GetLatLong = () => {

    let [latitude, setLatitude] = useState("")
    let [longitude, setLongitude] = useState("")

    const onPress = (event) => {
        const {geometry} = event

        setLatitude(geometry.coordinates[1])
        setLongitude(geometry.coordinates[0])
    }

    const renderBuble = () => {
        return(
            <View style={styles.bubleContainer}>
                <Text>Lat : {latitude}</Text>
                <Text>Long : {longitude}</Text>
            </View>
        )
    }

    return(
        <View>
            <View><Text>Tes Map</Text></View>
            <View style={styles.container}>
                <MapBox.MapView  style={styles.map} onPress={(e)=>onPress(e)}>
                    <MapBox.Camera zoomLevel={15} centerCoordinate={[106.84513, -6.21462]} />
                </MapBox.MapView>
                {renderBuble()}
            </View>
        </View>
    )
}

const styles= StyleSheet.create({
    page: {
        flex: 1,
    },
    container: {
        height: "100%",
        width:"100%",
    },
    map: {
        flex: 1
    },
    bubleContainer: {
        borderRadius:30,
        position: 'absolute',
        bottom:30,
        left:40,
        right:40,
        paddingVertical:16,
        minHeight:60,
        alignItems:'center',
        backgroundColor:'white'
    }

})

export default GetLatLong