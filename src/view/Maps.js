import React, {useEffect} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import MapBox from '@react-native-mapbox-gl/maps'

MapBox.setAccessToken("pk.eyJ1IjoiYm9tYnVyIiwiYSI6ImNrMWJneWZlbjBpZ2ozY252Yzdjd29tcGMifQ._boZnEnBaVHQqQCwOtY52Q")

const Maps = () => {

    return (
        <View style={styles.page}>
            <View><Text>Tes Map</Text></View>
            <View style={styles.container}>
                <MapBox.MapView  style={styles.map}/>
                <MapBox.Camera zoomLevel={9} centerCoordinate={[106.84513, -6.21462]} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    container: {
        height: "100%",
        width:"100%",
    },
    map: {
        flex: 1
    }
})


export default Maps
